const Block = require('./block'); 
const BlockChain = require('./blockchain').default;


const blockchain = new BlockChain("Hola a mi nueva cadena de bloques");
blockchain.addBlock("Data del segundo bloque");
blockchain.addBlock("Data del tercero bloque");
blockchain.addBlock("Data del cuarto bloque");
blockchain.addBlock("Data del quinto bloque");

console.log(blockchain);
