const SHA256 = require('crypto-js/sha256');

class Block{
   
    constructor(index, data, previousHash=''){
        this.index = index;
        this.data = data;
        this.previousHash = previousHash;
        this.date = new Date();
        this.hash = this.createdHash();
        this.nounce = 0;
    }

    createdHash(){
        const originalChain = `${this.index}${this.data}${this.date}${this.nounce}`;
        return SHA256(originalChain).toString();
    }

    mine(dif){
        while(!this.hash.toString().startsWith(dif)){
            this.nounce++;
            this.hash = this.createdHash();
        }
    }
}


module.exports = Block;