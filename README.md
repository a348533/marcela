# Blockchain

Este programa genera un Blockchain con loa siguientes atributos:
index: identificador de la posible cadena
data: el contenido del bloque
previousHash: valor del bloque anterior de la cadena
Asi mismo, mediante la clase Blockchain manipula elementos para poder generar la cadena de bloques. Finalmente tenemos el método mine que calcula el nuevo hash del bloque según su dificultad.


## Acknowledgements

 - [II. Conceptos básicos de las plataformas web
](https://aulas3.uach.mx/course/view.php?id=23801#section-2)



## Authors

- [@Marcela-15](https://github.com/Marcela-15)


## Appendix

Este es una tarea para la clase de desarrollo basado en plataformas.

## Deployment

Para correr este proyecto
bash
  node.js



## Features

- Calcula el nuevo hash del bloque según su dificultad


## Used By

Este proyecto esta en uso por:
- El profe Luis R.

## Support

For support, email a348533@uach.mx or join our Slack channel.
